#include "stdafx.h"
#include "Ellipse.h"


CEllipse::CEllipse() {

}


CEllipse::CEllipse(int a, int b, int c, int d)
{
	left = a;
	top = b;
	right = c;
	bottom = d;
}


CEllipse::~CEllipse()
{
}


CShape* CEllipse::Create(int a, int b, int c, int d) {
	return new CEllipse(a, b, c, d);
}


void CEllipse::Draw(HDC hdc) {
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	Ellipse(hdc, left, top, right, bottom);
}
