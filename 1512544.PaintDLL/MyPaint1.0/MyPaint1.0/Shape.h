#pragma once
#include "stdafx.h"


class CShape
{
public:
	CShape();
	virtual CShape *Create(int a, int b, int c, int d) = 0;
	virtual void Draw(HDC hdc) = 0;
	~CShape();
};

