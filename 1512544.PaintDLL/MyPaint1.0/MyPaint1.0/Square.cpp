#include "stdafx.h"
#include "Square.h"


CSquare::CSquare() {

}


CSquare::CSquare(int a, int b, int c, int d)
{
	left = a;
	top = b;
	right = c;
	bottom = d;
}


CSquare::~CSquare()
{
}


CShape* CSquare::Create(int a, int b, int c, int d) {
	return new CSquare(a, b, c, d);
}


void CSquare::Draw(HDC hdc) {
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	Rectangle(hdc, left, top, right, bottom);
}

