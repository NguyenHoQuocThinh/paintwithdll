#include "stdafx.h"
#include "Circle.h"


CCircle::CCircle() {

}


CCircle::CCircle(int a, int b, int c, int d)
{
	left = a;
	top = b;
	right = c;
	bottom = d;
}


CCircle::~CCircle()
{
}


CShape* CCircle::Create(int a, int b, int c, int d) {
	return new CCircle(a, b, c, d);
}


void CCircle::Draw(HDC hdc) {
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	Ellipse(hdc, left, top, right, bottom);
}