#include "stdafx.h"
#include "Rectangle.h"


CRectangle::CRectangle() {

}


CRectangle::CRectangle(int a, int b, int c, int d)
{
	left = a;
	top = b;
	right = c;
	bottom = d;
}


CRectangle::~CRectangle()
{
}

CShape* CRectangle::Create(int a, int b, int c, int d) {
	return new CRectangle(a, b, c, d);
}


void CRectangle::Draw(HDC hdc) {
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	Rectangle(hdc, left, top, right, bottom);
}