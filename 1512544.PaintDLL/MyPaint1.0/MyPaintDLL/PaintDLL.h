#pragma once

#ifdef MATHLIBRARY_EXPORTS
#define PAINTLIBRARY_API __declspec(dllexport) 
#else
#define PAINTLIBRARY_API __declspec(dllimport) 
#endif

namespace PaintLibrary {
	class CShape {
	protected:
		int type;
		int left;
		int right;
		int top;
		int bottom;
	public:
		PAINTLIBRARY_API virtual int GetType() = 0;
		PAINTLIBRARY_API virtual int GetLeft() = 0;
		PAINTLIBRARY_API virtual int GetTop() = 0;
		PAINTLIBRARY_API virtual int GetRight() = 0;
		PAINTLIBRARY_API virtual int GetBottom() = 0;
		PAINTLIBRARY_API virtual CShape *Create(int type, int a, int b, int c, int d) = 0;
		PAINTLIBRARY_API virtual void Draw(HDC hdc) = 0;
	};

	class CLine : public CShape {
	public:
	public:
		PAINTLIBRARY_API CLine();
		PAINTLIBRARY_API virtual int GetType();
		PAINTLIBRARY_API virtual int GetLeft();
		PAINTLIBRARY_API virtual int GetTop();
		PAINTLIBRARY_API virtual int GetRight();
		PAINTLIBRARY_API virtual int GetBottom();
		PAINTLIBRARY_API CLine(int type, int a, int b, int c, int d);
		PAINTLIBRARY_API CShape *Create(int type, int a, int b, int c, int d);
		PAINTLIBRARY_API void Draw(HDC hdc);
	};

	class CRectangle : public CShape {
	public:
	public:
		PAINTLIBRARY_API CRectangle();
		PAINTLIBRARY_API virtual int GetType();
		PAINTLIBRARY_API virtual int GetLeft();
		PAINTLIBRARY_API virtual int GetTop();
		PAINTLIBRARY_API virtual int GetRight();
		PAINTLIBRARY_API virtual int GetBottom();
		PAINTLIBRARY_API CRectangle(int type, int a, int b, int c, int d);
		PAINTLIBRARY_API CShape *Create(int type, int a, int b, int c, int d);
		PAINTLIBRARY_API void Draw(HDC hdc);
	};

	class CEllipse : public CShape {
	public:
	public:
		PAINTLIBRARY_API CEllipse();
		PAINTLIBRARY_API virtual int GetType();
		PAINTLIBRARY_API virtual int GetLeft();
		PAINTLIBRARY_API virtual int GetTop();
		PAINTLIBRARY_API virtual int GetRight();
		PAINTLIBRARY_API virtual int GetBottom();
		PAINTLIBRARY_API CEllipse(int type, int a, int b, int c, int d);
		PAINTLIBRARY_API CShape *Create(int type, int a, int b, int c, int d);
		PAINTLIBRARY_API void Draw(HDC hdc);
	};
}