#include "stdafx.h"
#include "PaintDLL.h"

namespace PaintLibrary {
	// Cline
	//=====================================================================================================================//
	CLine::CLine() {
		this->type = 0;
	}

	CLine::CLine(int inType, int inLeft, int inTop, int inRight, int inBottom) {
		this->left = inLeft;
		this->top = inTop;
		this->right = inRight;
		this->bottom = inBottom;
		this->type = inType;
	}

	CShape* CLine::Create(int inType, int inLeft, int inTop, int inRight, int inBottom) {
		return new CLine(inType, inLeft, inTop, inRight, inBottom);
	}

	void CLine::Draw(HDC hdc) {
		MoveToEx(hdc, this->left, this->top, NULL);
		LineTo(hdc, this->right, this->bottom);
	}

	int CLine::GetLeft() {
		return this->left;
	}

	int CLine::GetTop() {
		return this->top;
	}

	int CLine::GetRight() {
		return this->right;
	}

	int CLine::GetBottom() {
		return this->bottom;
	}

	int CLine::GetType() {
		return this->type;
	}
	//=====================================================================================================================//

	// CRectangle
	//=====================================================================================================================//
	CRectangle::CRectangle() {
		this->type = 1;
	}

	CRectangle::CRectangle(int inType, int inLeft, int inTop, int inRight, int inBottom) {
		this->left = inLeft;
		this->top = inTop;
		this->right = inRight;
		this->bottom = inBottom;
		this->type = inType;
	}

	CShape* CRectangle::Create(int inType, int inLeft, int inTop, int inRight, int inBottom) {
		return new CRectangle(inType, inLeft, inTop, inRight, inBottom);
	}

	void CRectangle::Draw(HDC hdc) {
		SelectObject(hdc, GetStockObject(NULL_BRUSH));
		Rectangle(hdc, left, top, right, bottom);
	}

	int CRectangle::GetLeft() {
		return this->left;
	}

	int CRectangle::GetTop() {
		return this->top;
	}

	int CRectangle::GetRight() {
		return this->right;
	}

	int CRectangle::GetBottom() {
		return this->bottom;
	}

	int CRectangle::GetType() {
		return this->type;
	}
	//=====================================================================================================================//

	// CEllipse
	//=====================================================================================================================//
	CEllipse::CEllipse() {
		this->type = 2;
	}

	CEllipse::CEllipse(int inType, int inLeft, int inTop, int inRight, int inBottom) {
		this->left = inLeft;
		this->top = inTop;
		this->right = inRight;
		this->bottom = inBottom;
		this->type = inType;
	}

	CShape* CEllipse::Create(int inType, int inLeft, int inTop, int inRight, int inBottom) {
		return new CEllipse(inType, inLeft, inTop, inRight, inBottom);
	}

	void CEllipse::Draw(HDC hdc) {
		SelectObject(hdc, GetStockObject(NULL_BRUSH));
		Ellipse(hdc, left, top, right, bottom);
	}

	int CEllipse::GetLeft() {
		return this->left;
	}

	int CEllipse::GetTop() {
		return this->top;
	}

	int CEllipse::GetRight() {
		return this->right;
	}

	int CEllipse::GetBottom() {
		return this->bottom;
	}

	int CEllipse::GetType() {
		return this->type;
	}
	//=====================================================================================================================//
}
